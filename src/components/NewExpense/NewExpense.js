import React, { useState } from "react";
import ExpandExpenseForm from "./ExpandExpensesForm";

import ExpenseForm from "./ExpenseForm";
import "./NewExpense.css";

const NewExpense = (props) => {
  const [showButton, setShowButton] = useState(false);

  const saveExpenseDataHandler = (enteredExpenseData) => {
    const expenseData = {
      ...enteredExpenseData,
      id: Math.random().toString(),
    };
    props.onAddExpense(expenseData);
  };

  return (
    <div className="new-expense">
      {showButton === false ? (
        <ExpandExpenseForm
          setShowButton={setShowButton}
          showButton={showButton}
        />
      ) : (
        <ExpenseForm
          onSaveExpenseData={saveExpenseDataHandler}
          setShowButton={setShowButton}
          showButton={showButton}
        />
      )}
    </div>
  );
};

export default NewExpense;

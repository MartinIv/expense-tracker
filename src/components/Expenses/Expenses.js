import React, { useState } from "react";

import ExpenseItem from "./ExpenseItem";
import Card from "../UI/Card";
import ExpensesFilter from "./ExpensesFilter";
import "./Expenses.css";
import ExpensesChart from "./ExpensesChart";

const Expenses = (props) => {
  const [filteredYear, setFilteredYear] = useState("2020");

  const filterChangeHandler = (selectedYear) => {
    setFilteredYear(selectedYear);
  };
  const filtered = props.items.filter((el) => {
    return el.date.getFullYear().toString() === filteredYear;
  });

  let expensesContent = <p className="expenses-filter">"No Expense Found"</p>;

  if (filtered.length > 0) {
    expensesContent = filtered.map((el) => {
      return (
        <ExpenseItem
          title={el.title}
          date={el.date}
          amount={el.amount}
          key={el.id}
        />
      );
    });
  }

  return (
    <div>
      <Card className="expenses">
        <ExpensesFilter
          selected={filteredYear}
          onChangeFilter={filterChangeHandler}
        />
        <ExpensesChart expenses={filtered} />
        {expensesContent}
      </Card>
    </div>
  );
};

export default Expenses;
